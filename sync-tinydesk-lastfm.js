'use strict';

/* eslint-disable no-console */
const PQueue = require('p-queue');
const Debug = require('debug');

const lastfm = require('./clients/lastfm');
const tinydesk = require('./db/tinydesk');

const info = Debug('app:sync:info');
const debug = Debug('app:sync:debug');
const error = Debug('app:sync:debug');

async function* fetchArtistPodcastInfo(podcastArtistGuesses) {
  const pqueue = new PQueue({
    interval: 60000,
    intervalCap: 300,
    carryoverConcurrencyCount: true,
    concurrency: 1,
  });

  const artistGuesses = Object.entries(podcastArtistGuesses);

  // async-iterators are very useful abstractions for serialized work, but
  // it does require bringing for loops back...
  for (const [artistName, podcasts] of artistGuesses) {
    debug(`Attempting to fetch info on ${artistName}`);
    if (!Array.isArray(podcasts)) throw new TypeError('Expecting podcast array');

    const artistResult = await pqueue.add(() => lastfm.artist.info({ artist: artistName }));

    const artist = artistResult.payload;

    if (!artist || !artist.name) {
      debug(`No artist found for ${artistName}. Skipping..`);
      continue;
    }
    debug(`Success! Now trying related artists...`);
    const relatedArtistsResult = await pqueue.add(
      () => lastfm.artist.similar({ artist: artist.name }),
    );

    if (!relatedArtistsResult.payload || !relatedArtistsResult.payload.length) {
      debug('No related artists found. Skipping..');
      continue;
    }

    const item = {
      artist: artistResult.payload,
      podcasts,
      relatedArtists: relatedArtistsResult.payload,
      lastfmResponses: { artistInfo: artistResult, relatedArtists: relatedArtistsResult },
    };
    yield item;
  }
}

async function syncArtistToPodcasts(db) {
  const topTrx = await db.getTrx(db);

  await db('related_artist').transacting(topTrx).del();
  await db('artist').transacting(topTrx).del();
  const podcastArtistGuesses = await tinydesk.guessTitlesFromArtists(topTrx);
  let i = 0;
  for await (const guess of fetchArtistPodcastInfo(podcastArtistGuesses)) {
    const trx = await db.getTrx(topTrx);

    try {
      const { podcasts, artist, relatedArtists } = guess;
      const artistId = await tinydesk.insertArtistInfo(trx)(artist, podcasts);
      debug(`Inserting related artists for ${artistId}`);
      await tinydesk.insertRelatedArtists(trx)(artistId, relatedArtists);
      i++;
      await trx.commit();
    } catch (e) {
      await trx.rollback();
      continue;
    }
  }
  await topTrx.commit();
  debug(`All done! Total synced: ${i}`);
  return { artistsSynced: i };
}

module.exports = syncArtistToPodcasts;
