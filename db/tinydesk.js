'use strict';

const Promise = require('bluebird');
const { groupBy } = require('ramda');

const tinydesk = { video: 'tinydesk_video', audio: 'tinydesk_audio' };


// use a transacting if provided

// query podcasts and try to remove any "Tiny Desk Podcast" extras which show up frequently
const guessTitlesFromArtists = async (db) => {
  const titleArtistSQL = `\
select id, title, guess_artist_from_title(title) as artist, 'video' as medium from tinydesk_video
union
select id, title, guess_artist_from_title(title) as artist, 'audio' as medium from tinydesk_audio;`;

  const results = await db.raw(titleArtistSQL);
  return groupBy(pod => pod.artist, results.rows);
};

// once artists are populated, assign to artist_id in tinydesk tables.
// used below when inserting artist info.
const assignArtistToPodcasts = db => (artistId, podcasts = []) =>
  Promise.map(podcasts, podcast => db(tinydesk[podcast.medium])
    .update({ artist_id: artistId })
    .where({ id: podcast.id }));

const insertArtistInfo = db => async (artistData, podcasts = []) => {
  const [artistId] = await db.insert(artistData).into('artist').returning('id');

  await assignArtistToPodcasts(artistId, podcasts);
  return artistId;
};

const insertRelatedArtists = db => async (artistId, relatedArtists) => {
  const toInsert = relatedArtists.map(related => ({ ...related, artist_id: artistId }));
  return db('related_artist').insert(toInsert);
};

module.exports = {
  guessTitlesFromArtists,
  assignArtistToPodcasts,
  insertArtistInfo,
  insertRelatedArtists,
};
