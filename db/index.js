'use strict';

const { mergeDeepRight } = require('ramda');
const knex = require('knex');
const conf = require('../conf');
const tinydesk = require('./tinydesk');
const { getTrx, maybeTransact } = require('./util');

const dbSecret = conf.get('db_password') ? { password } : {};

const defaultConfig = {
  client: 'postgresql',
  pool: conf.get('db:pool'),
  connection: {
    host: conf.get('db:host'),
    port: conf.get('db:port'),
    database: conf.get('db:database'),
    user: conf.get('db:user'),
    ...dbSecret,
  },
};

module.exports = (newConfig = {}) => {
  const config = mergeDeepRight(defaultConfig, newConfig);
  const db = knex(config);
  db.tinydesk = tinydesk;
  db.getTrx = getTrx;
  db.maybeTransact = maybeTransact;
  return db;
};
