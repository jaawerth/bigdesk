'use strict';

const { curryN } = require('ramda');

const maybeTransact = (knex, trx) => (trx ? knex.transacting(trx) : knex);
exports.maybeTransact = curryN(2, maybeTransact);

// evil cheat code
const getTrx = db => new Promise(resolve => db.transaction(trx => resolve(trx)));

exports.getTrx = getTrx;
