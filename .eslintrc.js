'use strict';

const varsIgnorePattern = '^_|^Promise';
const argsIgnorePattern = varsIgnorePattern;

module.exports = {
  extends: 'airbnb-base',
  env: {
    es6: true,
    node: true,
    commonjs: true,
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      impliedStrict: false,
    },
    ecmaVersion: 2018,
    sourceType: "script", // because CommonJS, not ES modules
  },
  plugins: [
    'react',
  ],
  rules: {
    'no-param-reassign': 0,
    quotes: [1, 'single', { avoidEscape: true, allowTemplateLiterals: true }],
    'implicit-arrow-linebreak': 0,
    'no-unused-vars': ["warn", { varsIgnorePattern, argsIgnorePattern }],
    semi: ["warn", "always", { "omitLastInOneLineBlock": true }],
    'no-shadow': 0,
    'object-curly-newline': ["error", { consistent: true, multiline: true }],
    'no-restricted-syntax': ["error", "WithStatement", "ForInStatement"],
    'no-await-in-loop':0,
    'no-loop-func': 0,
    'no-plusplus': 0,
    'no-continue': 0,
    'no-nested-ternary': 0,
  },
};
