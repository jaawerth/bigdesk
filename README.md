# bigdesk

Ultimately this is the start of what will eventually big an app that can surface recommended podcasts from NPR Music (starting with Tiny Desk Concert) by using music services to find related artists, and ultimately linking to music streaming services via OAUTH to see users' favorite music. A bit too ambitious for a 1-2 hour out of work project, but the data modeling is largely done and the syncing code is about complete, after which a simple API and basic interface can follow.


## Database
The database is postgresql and making use of the [multicorn](https://multicorn.org/) extension for creation of [Foreign Data Wrappers](https://wiki.postgresql.org/wiki/Foreign_data_wrappers), which we're using here to create a live table capable of pulling from the Tiny Desk Concert RSS feeds (both video and audio).

Once the database is built, it runs a function that reads the feed and stores a transformed version in `tinydesk_audio` and `tinydesk_video` tables.

For DB initialization, see [docker-pg/docker-entrypoint-initdb.d/](docker-pg/docker-entrypoint-initdb/), which the postgres docker image automatically sources when initialized and no tables exist.


### Building/retrieving the DB container

#### Pull existing images from this repository's Docker registry

```bash

# just using docker-compose to pull
docker-compose pull
docker-compose up

# using docker for individual images
docker pull registry.gitlab.com/jaawerth/bigdesk/bigdesk-db
docker pull registry.gitlab.com/jaawerth/bigdesk/bigdesk-app

# run database container and remove container when done
docker run -p 15432:5432 -v --name bigdesk_pg --rm -d registry.gitlab.com/jaawerth/bigdesk/bigdesk-db
```

#### Build it yourself

```bash
docker-compose up --build
```
## Node

I got a bit bogged down here getting the lastfm + db sync working - this is what made this too ambitious for an out-of-work project that was only supposed to take a couple hours! Here are some highlights:

### Config
- [./config/app.ini](config/app.ini) and [conf.js](conf.js) show my overall strategy for setting up application config.
- I generally use a more robust format than `.ini`, like `toml` or `yaml`, but I tend to use the simplest format that makes sense for a given application's complexity.

### Secrets

Secrets go into a gitignored `.secrets`, `.secrets.ini`, or `.secrets.toml` file and **NOT** .env, since the latter is sourced by too many things and can lead to them being embedded in env vars, which can leak. I stick to very simple formats for this, so they can also be parsed by cli tools like `awk` during bash scripts, e.g. This application is built 

`config/.secrets.ini` needs to be preset for lastfm requests to work


```ini
## create secrets file
cat <<EOF > ./config/.secrets.ini
lastfm_api_key=<your api key>

## could also just use PGPASSWORD for the following, or a .pgpass file
## in this case, the db should let one log in directly via peer authentication, at least via psql -d bigdesk -U bigdesk -h 127.0.0.1 -p 15432
# db_password=<your db password>
EOF
```

### Parameterized middleware and stateful tools (e.g. database pool)

- See [db/index.js](db/index.js) and [db/tinydesk.js](db/tinydesk.js). This makes testing easier, as well as building out larger application architectures, as the params can optionally be passed from the top down (in this case I use values sourced from the config file as default function params that can be overridden)

### Separate out app.js from what launches the server
[./bin/www.js'](./bin/www.js) is a good example of this, though the server isn't quite in a state where it can run. It requires [app.js](app.js) and spins up a server. This allows configurable options for working without a reverse proxy like nginx;

### API clients
See [clients/lastfm.js](clients/lastfm.js) and [clients/napster.js](clients/napster.js), both of which can be 

### Syncing logic

A significant amount of time was spent here. See [./sync-tinydesk-lastfm](sync-tinydesk-lastfm), and [bin/sync-lastfm.js](bin/sync-lastfm.js). All syncing should be running in a transaction, and requests against lastfm are being rate-limited.
