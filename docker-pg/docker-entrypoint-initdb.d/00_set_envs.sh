
export DATABASE="${DATABASE:-bigdesk}"
export DBUSER="${DBUSER:-$DATABASE}"

if [ -z $DBPASSWORD ]; then
  DBPASSWORD=$(pwgen 14 1);
fi

psql -v ON_ERROR_STOP=1 <<-EOSQL
  create user $DBUSER password \$PW\$$DBPASSWORD'\$PW\$;
  create database $DATABASE owner $DBUSER;
EOSQL
