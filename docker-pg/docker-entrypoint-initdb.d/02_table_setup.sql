\set targetdb `echo $DATABASE`
\connect :targetdb

-- set up FDW server that pulls from RSS feeds
create server rss foreign data wrapper multicorn options (
  wrapper 'multicorn.rssfdw.RssFdw'
);


create table artist (
  id bigserial primary key,
  mbid text unique, -- MusicBrainz ID
  name text not null,
  alias text,
  created_on timestamptz not null default now(),
  updated_on timestamptz not null default now(),
  lastfm_meta jsonb
);
create index on artist using gin (name gin_trgm_ops);
create index on artist using gin (name gin_trgm_ops);

create table related_artist (
  artist_id bigint not null references artist(id),
  artist_mbid text references artist(mbid),
  related_name text not null,
  related_mbid text,
  lastfm_match decimal,
  lastfm_meta jsonb
);
create index on related_artist(artist_id);
create index on related_artist(lastfm_match);
create index on related_artist(artist_mbid);
create index on related_artist(related_mbid);
create index on related_artist using gin (related_name gin_trgm_ops);

-- create Tiny Desk audio FDW
create foreign table fdw_tinydesk_audio (
  guid text,
  "pubDate" timestamp,
  title text,
  description text,
  author text,
  duration text,
  link text,
  "itunes:title" text,
  "itunes:author" text,
  "itunes:summary" text,
  "itunes:duration" int,
  "itunes:image/@href" text,
  "enclosure/@url" text
) server rss options (url 'https://www.npr.org/rss/podcast.php?id=510306' );

-- create Tiny Desk video FDW
create foreign table fdw_tinydesk_video (
  guid text,
  "pubDate" timestamp,
  title text,
  description text,
  author text,
  duration text,
  link text,
  "itunes:title" text,
  "itunes:author" text,
  "itunes:summary" text,
  "itunes:duration" int,
  "itunes:image/@href" text,
  "enclosure/@url" text
) server rss options (url 'https://www.npr.org/rss/podcast.php?id=510292' );

-- create the actual table for storing cached/indexed data
create table tinydesk_audio (
  id serial primary key,
  artist_id bigint references artist(id),
  guid text unique,
  pub_date timestamptz,
  updated_on timestamptz default now(),
  title text,
  author text,
  description text,
  link text,
  duration int,
  image_href text
);
create index on tinydesk_audio using gin (title gin_trgm_ops);

create table tinydesk_video (
  id serial primary key,
  artist_id bigint references artist(id),
  guid text unique,
  pub_date timestamptz,
  updated_on timestamptz default now(),
  title text,
  author text,
  description text,
  link text,
  duration int,
  image_href text
);
create index on tinydesk_audio using gin (title gin_trgm_ops);

-- Functions for updating the cache whenever necessary.
-- They use upsert so new items are added or existing items updated if the guid matches.

create function update_tinydesk_audio() RETURNS SETOF text AS $$
  insert into tinydesk_audio (guid, pub_date, title, author, description, link, duration, image_href)
  select guid,
    "pubDate"::timestamp as pub_date,
    COALESCE (title, "itunes:title") as title,
    COALESCE (author, "itunes:author") as author,
    COALESCE (description, "itunes:summary") as description,
    COALESCE (link, "enclosure/@url") as link,
    COALESCE (duration::integer, "itunes:duration") as duration,
    "itunes:image/@href" as image_href
  from fdw_tinydesk_audio
  on conflict (guid) do update
  set
    pub_date = EXCLUDED.pub_date,
    title = EXCLUDED.title,
    author = EXCLUDED.author,
    description = EXCLUDED.description,
    link = EXCLUDED.link,
    duration = EXCLUDED.duration,
    image_href = EXCLUDED.image_href,
    updated_on = now()
  RETURNING guid;
$$ language SQL;


create function update_tinydesk_video() RETURNS SETOF text AS $$
  insert into tinydesk_video (guid, pub_date, title, author, description, link, duration, image_href)
  select guid,
    "pubDate"::timestamp as pub_date,
    COALESCE (title, "itunes:title") as title,
    COALESCE (author, "itunes:author") as author,
    COALESCE (description, "itunes:summary") as description,
    COALESCE (link, "enclosure/@url") as link,
    COALESCE (duration::integer, "itunes:duration") as duration,
    "itunes:image/@href" as image_href
  from fdw_tinydesk_video
  on conflict (guid) do update
  set
    pub_date = EXCLUDED.pub_date,
    title = EXCLUDED.title,
    author = EXCLUDED.author,
    description = EXCLUDED.description,
    link = EXCLUDED.link,
    duration = EXCLUDED.duration,
    image_href = EXCLUDED.image_href,
    updated_on = now()
  RETURNING guid;
$$ language SQL;

create function set_updated_on_trigger() returns trigger as $$
  begin
    new.updated_on = now();
    return new;
  end;
$$ language plpgsql;

create trigger trigger_updated_on before insert or update on tinydesk_audio
for each row execute procedure set_updated_on_trigger();

create trigger trigger_updated_on before insert or update on tinydesk_video
for each row execute procedure set_updated_on_trigger();

create function sync_tinydesk() returns bigint as $$
  select update_tinydesk_audio()
  union
  select update_tinydesk_video();
  select setval('tinydesk_audio_id_seq', (select max(id) from tinydesk_video) + 1);
  select setval('tinydesk_audio_id_seq', (select max(id) from tinydesk_audio) + 1);
$$ language sql;

