#!/bin/bash

# DATABASE="${DATABASE:-bigdesk}"
# DBUSER="${DBUSER:-$DATABASE}"

psql -v ON_ERROR_STOP=1 -d $DATABASE <<-EOSQL

  -- Next: set up default privileges
  -- normally we'd go for principle of least privilege and have separate admin/app roles,
  -- but this is a quick setup job

  alter default privileges in schema public grant all privileges on tables to $DBUSER;
  alter default privileges in schema public grant all privileges on sequences to $DBUSER;
  alter default privileges in schema public grant select on tables to $DBUSER;

  -- set up extensions
  create extension multicorn; -- Convenience library for FDWs (Foreign Data Wrappers)
  create extension pg_trgm;   -- Fuzzy text matching

EOSQL
