\set  targetdb `echo $DATABASE`
\connect :targetdb

create function guess_artist_from_title(title text) returns text as $$
begin
  return regexp_replace(title,  '\s*:?\s*(Holiday)?\s*Tiny\s*Desk\s*Concert\s*$', '', 'i');
end;
$$
language plpgsql;
