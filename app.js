'use strict';

/** app definition
 * This defines and exports the express app, but does not start it.
 * This allows easier testing and more flexible deployment. See bin/www.js for
 * where the app is actually started.
 */
const express = require('express');
const body = require('body-parser');
const expressPinoLogger = require('express-pino-logger');
const conf = require('./conf');
const logger = require('./log');
const db = require('./db');
const apiRoutes = require('./api')({ db, conf });

const app = express();

// basic middleware setup
app.use(body.json({ type: ['*/json', 'text/*'] }));
app.use(body.urlencoded({ extended: true }));
app.use(expressPinoLogger({ logger: logger.child({ type: 'access' }) }));


// mount api routes on /api
app.use('/api', apiRoutes);

// fallback
app.all('*', (req, res) => {
  res.status(404).json({ error: true, message: 'path not found' });
});

/** Shutdown handler
 * When the app gets a SIGINT or SIGTERM (see bin/www.js), app:shutdown is
 * emitted on the app's EventEmitter. This triggers a graceful shutdown, which
 * also allows gor seamless reload when using forked/cluster mode.
 */
app.once('app:shutdown', () => {
  logger.info('Stopping knex...');
  db.destroy()
    .then(() => app.emit('app:shutdown:done'))
    .catch((err) => {
      logger.error('Error destroying knex pool', err);
      app.emit('app:shutdown:error', err);
    });
});

module.exports = app;
