'use strict';

const path = require('path');
const conf = require('nconf');
const ini = require('ini');

// Normalize path to look in config dir. Aboluste path will still work.
const confPath = filePath => path.resolve(__dirname, 'config', filePath);

conf
  .argv()
  .env({
    separator: '__',
    lowerCase: true,
    parseValues: true,
    match: /^(?!env$)/i,
  })
  .file('app', { file: confPath('app.ini'), format: ini })
  .file('secrets', { file: confPath('.secrets.ini'), format: ini });

module.exports = conf;
