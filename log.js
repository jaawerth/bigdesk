'use strict';

const pino = require('pino');
const conf = require('./conf');

const level = conf.get('log:level') || 'debug';

const logger = pino({ level });

module.exports = logger;
