'use strict';

const axios = require('axios');
const debug = require('debug');
const { tap, pathOr } = require('ramda');
const conf = require('../conf');

const defaults = {
  baseURL: conf.get('lastfm:base_url'),
  params: {
    api_key: conf.get('lastfm_api_key'),
    format: 'json',
    autocorrect: 1,
  },
};
const lastfm = axios.create(defaults);

lastfm.interceptors.request.use(tap(debug('lastfm:request')));
lastfm.interceptors.response.use(tap(debug('lastfm:response')));

const artist = {
  /**
   * info - Request artist info via GET request to last.fm API
   *
   * @param {Object} query - URL query params for artist lookup
   * @param {string} [query.artist] - Artist name to use for lookup.
   * LastFM will attempt to autocorrect
   * @param {string|number} [query.mbid] - MusicBrainz ID of artist. Can be used instead of name.
   * @param {Object} [options = {}]
   *
   * @returns {Promise} - axios result object with added .payload property transformed response body
   */
  async info({ artist, _mbid, ...otherParams }, opts = {}) {
    const searchParams = artist
      ? { artist } : _mbid
      ? { mbid: _mbid } : {}; // eslint-disable-line indent
    const params = { ...otherParams, ...searchParams, method: 'artist.getInfo' };
    const result = await lastfm({ ...opts, params });
    const { name, mbid, ...lastfmMeta } = result.data.artist || {};
    return Object.assign(result, { payload: { name, mbid, lastfm_meta: lastfmMeta } });
  },

  /**
   * similar - Search for similar artists with a "match" score
   * @param {Object} query - URL query params for artist lookup
   * @returns {Promise} - The axios response object containing .data w/ response body
   *                      and .similar prop containing array of similar artists
   */
  async similar({ artist, mbid, ...otherParams }, opts = {}) {
    const searchParams = artist ? { artist } : mbid ? { mbid } : {};
    const params = { ...otherParams, ...searchParams, method: 'artist.getSimilar' };

    const result = await lastfm({ ...opts, params });
    const similarArtists = pathOr([], ['similarartists', 'artist'], result.data);
    result.payload = similarArtists.map(({ name, mbid, match, ...lastfmMeta }) => ({
      related_name: name,
      related_mbid: mbid,
      lastfm_match: match,
      lastfm_meta: lastfmMeta,
    }));
    return result;
  },
};

lastfm.artist = artist;
module.exports = lastfm;
