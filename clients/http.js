'use strict';

const got = require('got');
const debug = require('debug');
const { compose, into, map } = require('transduce');
const { groupBy } = require('ramda');

const toObj = into({});

const wrap = fn => (value) => {
  fn(value);
  return value;
};



// const { map, tap } = require('ramda');

const debugHookKeys = {
  init: 'init',
  beforeRequest: 'request',
  beforeRedirect: 'redirect',
  beforeRetry: 'retry',
  afterResponse: 'response',
  beforeError: 'error',
};


const mkDebugHooks = prefix => into({}, compose(
  map(([hook, key]) => [hook, wrap(debug(`${prefix}:${key}`))]),
  groupBy(([hook, _func]) => hook),
  map()
), Object.entries(debugHookKeys));

const defaultHooks = mkDebugHooks('got');
console.log(defaultHooks);
