#!/usr/bin/env node
'use strict';

const debug = require('debug')('app:sync');
const error = require('debug')('app:sync:error');
const initailizeDB = require('../db'); // initialize db pool
const sync = require('../sync-tinydesk-lastfm');

(async function go() {
  const db = initailizeDB();
  try {
    await sync(db);
    debug('Sync finished!');
  } catch (err) {
    error(err);
  }
  await db.destroy();
}()).then(res => {
  debug('Should be ready to close');
});
