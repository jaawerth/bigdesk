#!/usr/bin/env node

'use strict';

/** Server setup
 * In more complex apps, may contain optional https/redirects, particularly
 * if not using nginx or a similar proxy/balancer in front of node.
 */
const http = require('http');
const exitHook = require('async-exit-hook');
const log = require('../log');
const conf = require('../conf');
const app = require('../app');

const HTTP_PORT = conf.get('http_port');

const httpServer = http.createServer(app);
httpServer.listen(HTTP_PORT, () => {
  log.info(`http listening on ${HTTP_PORT}`);
});


/** Graceful app shutdown
 * Stops server accepting new connections, letting existing requests complete.
 * Emits an event that triggers app to spin down its db client pool
 * and any others ervices (see app.js)  so it can gracefully shutdown/reload.
 */
const closeServer = server => new Promise((resolve, reject) => {
  server.close((err) => {
    if (err) return reject(err);
    return resolve();
  });
});

const closeApp = targetApp => new Promise((resolve, reject) => {
  targetApp.once('app:shutdown:done', () => {
    log.debug('Servers closed, stopping app services');
    resolve();
  });
  targetApp.once('app:shutdown:error', (err) => {
    log.error('Error stopping app services');
    reject(err);
  });
  targetApp.emit('app:shutdown');
});

const shutdownHandlers = {
  done: callback => () => {
    log.debug('Done stopping app services; ready to close!');
    callback();
  },
  error: callback => (err) => {
    log.error('Error shutting down: ', err);
    callback();
  },
};

const shutdown = (callback) => {
  log.info('SIGINT/SIGTERM detected, process shutting down; closing server and relevant services');
  closeServer(httpServer).then(() => closeApp(app))
    .then(shutdownHandlers.done(callback))
    .catch(shutdownHandlers.error(callback));
};
exitHook(shutdown);
